# bhvn-mixxx-vci400

Vestax VCI-400 Mapping v1.0 [BHVN Official]

# Description

A VCI-400 mapping from scratch written for Mixxx 2.2-2.4. 

# Notes

This mapping is largely done to my personal taste. Because there is not native
support for the Shift button in Mixxx 2.2.x, remapping to your personal taste 
may require editing the scripts manually.

Transport

Standard Slip/Play/Cue controls, with no shift action. Platters use Mixxx’s builtin spindown handlers. Vinyl button enables touch-to-scratch.

Mixer/PFL/eq are mapped in a standard boring manner.

* Shift-load to eject
* Sync button is a bog standard Mixxx sync button using the new paradigm

I started to write fancy sync button code, then realized I don’t care because I never turn on quantize anyway.

There are four pages for the pads:

* hotcues
* loops
* loop-rolls, (note: the first pad is reverse/censor)
* sampler, implemented as a “play until release” sampler

The left encoder is pitch control:

* Turn: half step adjust
* Push: match pitch
* Shift-Turn: quarter-step adjust
* Shift-Push: reset track pitch

Right encoder is beatjump/beatloop control:

* Turn: beatjump
* Push: set/clear beatloop (also clears current manual loop)
* Shift-Turn: loop-size adjust (and beatloop size adjust)

FX knobs and buttons are all mapped to FX magic controls, which currently all map 
to FX blends in Mixxx. This hopefully will be more interesting in the future.

The bonus buttons above the pitch faders map to pitch-lock for various decks.

The browse knob navigates up/down, shift for page-up and -down. Push loads into 
the first available. I didn’t map anything to “show big browser” because the 
spacebar is huge! However it should be easy to remap the browse knob, as it has 
no shift function.

The four special buttons in the center:

* top-left: split cue
* top-right: auto-dj (shift) fade now
* bottom-left: Tab (shift-tab) navigation
* bottom-right: add to auto-dj (shift add-next)

The tiny slider in the center is still looking for a purpose. I still love it.

The dedicated “master mix to headphones” button is not yet implemented. Sorry.
