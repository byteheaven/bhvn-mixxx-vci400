// Script support for Mixxx -> Vestax VCI400 mapping
// BHVN edition v1.0.5-SNAPSHOT
// Copyright Tom Surace: 2020-2024
// Licensed under the WTFPL - share and enjoy.
// Bugfixes, altnernate mappings, and enhancement feedback welcomed.
//
// Developed using the Serato firmware and a "regular" edition VCI-400


// -- Configuration

// Scratch tuning
//
// Default/recommended values are
//   alpha: 1/8
//   beta: alpha / 32
//
// May wish to tune based on small wheel diameter and taste, but
// honestly the default values feel great.

const SCRATCH_ALPHA = 1.0 / 8;
const SCRATCH_BETA = SCRATCH_ALPHA / 32;

// Here's the deal: the VCI wheels are a litle smaller diameter-wise
// than a CDJ and a LOT smaller than a 45 or 33. So, let's turn up the
// scratch rate (by setting the ticks multiplier to less than 1.0).

const SCRATCH_TICK_MULTIPLIER = 0.65;

// -- Constants

/**
 * Jog wheel correction. Mixxx is referenced to a wheel that's 
 * 256/rev, the VCI400 is 2400/rev. 256/2400 = 0.10667
 */
const JOG_CORRECTION = 256 / 2400;

/**
 * A simple led that can be turned on or off
 *
 * @param channel is the 0-indexed midi channel on which we will send.
 *   0 = channel 1, etc.
 * @param ctrl controller ID for LED control byte
 */
const LED = function(channel, ctrl) {
    /**
     * Status byte to send. It's a note-on/off message so...
     */
    this.status = 0x90 + channel;
    this.ctrl = ctrl;
    this.isOn = false;

    /**
     * If set to false, the LED will continue to track state,
     * but not send MIDI on state changes.
     */
    this.isActive = true;
};

/**
 * flush value to MIDI immediately, regardless of isActive
 */
LED.prototype.send = function() {
    var value;
    if (this.isOn) {
        value = 0x7f;  // Pretty much all LEDs on this controller work the same
    }
    else {
        value = 0x00;
    }

    midi.sendShortMsg(this.status, this.ctrl, value);
};

/**
 * Set LED to new value and possibly update control surface. Value
 * is send if isActive === true.
 *
 * @param value boolean value: true for on.
 */
LED.prototype.set = function(value) {
    this.isOn = value;
    if (this.isActive) {
        this.send();
    } 
}

/**
 * Toggles the state of an LED, and returns the new state
 */
LED.prototype.toggle = function() {
    this.isOn = !this.isOn;
    if (this.isActive) {
        this.send();
    }

    return this.isOn;
};

/**
 * Extend LED to be a tempo-based blinky by hooking
 * into the beat_active signal for the indicated
 * deck group
 */
const TempoLED = function(channel, control, group) {
    LED.call(this, channel, control);

    this.group = group;

    /**
     * If counter > 0, is either on or waiting to become active
     */
    this._counter = 0;
};

TempoLED.prototype = Object.create(LED.prototype);

//
// Initially aim for a 20ms on-time centered on the beat.
// This does not account for MIDI-out lag. :(
// 
// Experimentation shows that because of the lax timing of timer callbacks 
// (obviously for performance reasons) blink_ticks = 1 means sometimes a light
// doesn't really get turned on at all. :)
//
TempoLED.TICK_MS = 20;       // Tick speed
TempoLED.INITIAL_TICKS = 3;  // Delay = INITIAL - BLINK - 1
TempoLED.BLINK_TICKS = 2;    // stays on for this many ticks

// Animate all TempoLEDs using a single timer for efficiency

/**
 * Array of all TempoLEDs, for animation.
 */
TempoLED._ledRegistry = [];
TempoLED._globalTick = function() {
    for (var i = 0; i < TempoLED._ledRegistry.length; i++) {
	TempoLED._ledRegistry[i].tick();
    }
};

TempoLED._animationTimer = engine.beginTimer(TempoLED.TICK_MS, function() {
        TempoLED._globalTick();
    },
    false /* one-shot */ );

TempoLED.prototype.beatActive = function(value, group, controlName) {
    if (value === 1) {
        this._counter = TempoLED.INITIAL_TICKS;
    }
};

/**
 * Animation timer callback.
 */
TempoLED.prototype.tick = function() {
    if (!this.isActive) {  // Don't blink while inactive!
        return;
    }

    if (this._counter === 0) {
        return;
    }

    -- this._counter;

    // do a "this.send" but with the inverse value while beat_active.
    // The sent value is not stored.

    var isOn = this.isOn;
    if (this._counter > TempoLED.BLINK_TICKS) { // don't blink yet!
        return;
    }
    else if (this._counter > 0) {               // Within the blink window?
        isOn = !isOn;
    }
    else {                                      // Blink is ended!
        ; // do nothing
    }

    var value;
    if (isOn) {
        value = 0x7f;
    }
    else {
        value = 0x00;
    }

    midi.sendShortMsg(this.status, this.ctrl, value);
};

TempoLED.prototype.init = function() {
    engine.makeConnection(this.group, 'beat_active', (value, group, controlName) => {
        this.beatActive(value, group, controlName);
    });

    TempoLED._ledRegistry.push(this);
};

/**
 * Meters!
 */
const VuController = function(status, ctrl) {
    this.status = status;
    this.ctrl = ctrl;

    /**
     * MIDI spam optimization
     */
    this.level = -1;
};

VuController.prototype.init = function(group, control) {
    midi.sendShortMsg(this.status, this.ctrl, 0x00);
    engine.makeConnection(group, control, (value, group, name) => { 
        return this.handleLevel(value, group, name);
    });
}

VuController.prototype.handleLevel = function(value, group, controlName) { 
    // VU meter notes: 
    // 
    // LED1 always on
    // 12 - LED 2
    // 24 - LED 3
    // 116 = clip light (LED 11)
     
    // MIDI spam optimization: only send on change.

    value = Math.floor(value * 0x7f); // Range from 0-127
    if (value !== this.level) {
        this.level = value;
        midi.sendShortMsg(this.status, this.ctrl, value);
    }
};

/**
 * Common low-level state not tied to a specific deck etc
 */
const GlobalState = function() {
    /**
     * Is the shift button held?
     */
    this.isShifted = false;
};

/**
 * Base type for the various trigger pad pages
 */
const PageBase = function(channel, group, globalState) {
    this.group = group;
    this.globalState = globalState;

    this.led1 = new LED(channel, 0x07);
    this.led2 = new LED(channel, 0x08);
    this.led3 = new LED(channel, 0x09);
    this.led4 = new LED(channel, 0x0A);
    this.led5 = new LED(channel, 0x0B);
    this.led6 = new LED(channel, 0x0C);
    this.led7 = new LED(channel, 0x0D);
    this.led8 = new LED(channel, 0x0E);

    this.deactivate();
};

PageBase.prototype.init = function() {
    this.led1.send();
    this.led2.send();
    this.led3.send();
    this.led4.send();
    this.led5.send();
    this.led6.send();
    this.led7.send();
    this.led8.send();
};

/**
 * Pages could, theoretically, disconnect event handlers
 * in deactivate, but that seems unnecessary.
 *
 * all pages should ensure that held-buttons are released
 *   when deactivate is called.
 */
PageBase.prototype.deactivate = function() {
    this.led1.isActive = false;
    this.led2.isActive = false;
    this.led3.isActive = false;
    this.led4.isActive = false;
    this.led5.isActive = false;
    this.led6.isActive = false;
    this.led7.isActive = false;
    this.led8.isActive = false;
};

PageBase.prototype.activate = function() {
    this.led1.isActive = true;
    this.led2.isActive = true;
    this.led3.isActive = true;
    this.led4.isActive = true;
    this.led5.isActive = true;
    this.led6.isActive = true;
    this.led7.isActive = true;
    this.led8.isActive = true;
};

/**
 * Force sending all led values immediately
 */
PageBase.prototype.sendAllLeds = function() {
    this.led1.send();
    this.led2.send();
    this.led3.send();
    this.led4.send();
    this.led5.send();
    this.led6.send();
    this.led7.send();
    this.led8.send();
};

PageBase.prototype._padBtnMsg = function(padNum, value) {
    // do nothing - derived classes must implement
};

/**
 * Process a trigger pad button.
 *
 * Calls "this._padBtnMsg()", which can be overridden in derived 
 * classes.
 */
PageBase.prototype.handleTriggerPadBtn = function(channel, control, value, status, group) {
    // "control" should be in the range: [0x07,0x0e], for pads 0-7

    if (control < 0x07 || control > 0x0e) {
        throw new Error("Trigger pad control ID out of range: " + control);
    }
    var padNum = control - 0x06; // 1-8
    this._padBtnMsg(padNum, value);
};

/**
 * Models the pads/lights/knobs when the cue page is active
 */
const CuePage = function(channel, group, globalState) {
    PageBase.call(this, channel, group, globalState);
};

CuePage.prototype = Object.create(PageBase.prototype);

CuePage.prototype.init = function() {
    PageBase.prototype.init.call(this);

    var _this = this;

    // Make connections between hotcue state and the LEDs. This is a bit verbose but
    // it seems silly to make a one-line function for each hotcue, and stupid
    // efficient to do string matching for this. So here we go.

    engine.makeConnection(this.group, 'hotcue_1_status',
        function(value, group, controlName) { _this.led1.set(value === 1); });
    engine.makeConnection(this.group, 'hotcue_2_status',
        function(value, group, controlName) { _this.led2.set(value === 1); });
    engine.makeConnection(this.group, 'hotcue_3_status',
        function(value, group, controlName) { _this.led3.set(value === 1); });
    engine.makeConnection(this.group, 'hotcue_4_status',
        function(value, group, controlName) { _this.led4.set(value === 1); });
    engine.makeConnection(this.group, 'hotcue_5_status',
        function(value, group, controlName) { _this.led5.set(value === 1); });
    engine.makeConnection(this.group, 'hotcue_6_status',
        function(value, group, controlName) { _this.led6.set(value === 1); });
    engine.makeConnection(this.group, 'hotcue_7_status',
        function(value, group, controlName) { _this.led7.set(value === 1); });
    engine.makeConnection(this.group, 'hotcue_8_status',
        function(value, group, controlName) { _this.led8.set(value === 1); });
};

CuePage.prototype.deactivate = function() {
    PageBase.prototype.deactivate.call(this);

    // release all buttons here
    engine.setValue(this.group, 'hotcue_1_activate', 0); // prevent stuck activate
    engine.setValue(this.group, 'hotcue_2_activate', 0); // prevent stuck activate
    engine.setValue(this.group, 'hotcue_3_activate', 0); // prevent stuck activate
    engine.setValue(this.group, 'hotcue_4_activate', 0); // prevent stuck activate
    engine.setValue(this.group, 'hotcue_5_activate', 0); // prevent stuck activate
    engine.setValue(this.group, 'hotcue_6_activate', 0); // prevent stuck activate
    engine.setValue(this.group, 'hotcue_7_activate', 0); // prevent stuck activate
    engine.setValue(this.group, 'hotcue_8_activate', 0); // prevent stuck activate
};

CuePage.prototype._padBtnMsg = function(padNum, value) {
    if (value !== 0x7f) {
        engine.setValue(this.group, "hotcue_" + padNum + "_activate", 0); // prevent stuck activate
        return;
    }

    if (this.globalState.isShifted) {
        engine.setValue(this.group, "hotcue_" + padNum + "_clear", 1);
        engine.setValue(this.group, "hotcue_" + padNum + "_clear", 0);
    }
    else {
        engine.setValue(this.group, "hotcue_" + padNum + "_activate", 1);
    }
}

/**
 * Instant beatloop page
 */
const LoopPage = function(channel, group, globalState) {
    PageBase.call(this, channel, group, globalState);
};

LoopPage.prototype = Object.create(PageBase.prototype);

LoopPage.prototype.init = function() { 
    PageBase.prototype.init.call(this);

    var _this = this;    

    engine.makeConnection(this.group, 'beatloop_0.5_enabled',
        function(value, group, controlName) { _this.led1.set(value === 1); });
    engine.makeConnection(this.group, 'beatloop_1_enabled',
        function(value, group, controlName) { _this.led2.set(value === 1); });
    engine.makeConnection(this.group, 'beatloop_2_enabled',
        function(value, group, controlName) { _this.led3.set(value === 1); });
    engine.makeConnection(this.group, 'beatloop_4_enabled',
        function(value, group, controlName) { _this.led4.set(value === 1); });
    engine.makeConnection(this.group, 'beatloop_8_enabled',
        function(value, group, controlName) { _this.led5.set(value === 1); });
    engine.makeConnection(this.group, 'beatloop_16_enabled',
        function(value, group, controlName) { _this.led6.set(value === 1); });
    engine.makeConnection(this.group, 'beatloop_32_enabled',
        function(value, group, controlName) { _this.led7.set(value === 1); });
    engine.makeConnection(this.group, 'beatloop_64_enabled',
        function(value, group, controlName) { _this.led8.set(value === 1); });
};

LoopPage.prototype._padBtnMsg = function(padNum, value) {
    if (value !== 0x7f) { // ignore button release
        return;
    }

    var control;

    switch (padNum) {
    case 1: control = 'beatloop_0.5_toggle';  break;
    case 2: control = 'beatloop_1_toggle';  break;
    case 3: control = 'beatloop_2_toggle';  break;
    case 4: control = 'beatloop_4_toggle';  break;
    case 5: control = 'beatloop_8_toggle';  break;
    case 6: control = 'beatloop_16_toggle'; break;
    case 7: control = 'beatloop_32_toggle'; break;
    case 8: control = 'beatloop_64_toggle'; break;
    default: throw new Error("unexpected case in switch");
    }

    engine.setValue(this.group, control, 1); // press
    engine.setValue(this.group, control, 0); // release now
}

const LoopRollPage = function(channel, group, globalState) {
    PageBase.call(this, channel, group, globalState);
};

LoopRollPage.prototype = Object.create(PageBase.prototype);

LoopRollPage.prototype.init = function() {
    PageBase.prototype.init.call(this);

    var _this = this;

    engine.makeConnection(this.group, 'reverseroll',
        function(value, group, controlName) { _this.led1.set(value === 1); });
    engine.makeConnection(this.group, 'beatloop_0.0625_enabled',
        function(value, group, controlName) { _this.led2.set(value === 1); });
    engine.makeConnection(this.group, 'beatloop_0.125_enabled',
        function(value, group, controlName) { _this.led3.set(value === 1); });
    engine.makeConnection(this.group, 'beatloop_0.25_enabled',
        function(value, group, controlName) { _this.led4.set(value === 1); });
    engine.makeConnection(this.group, 'beatloop_0.5_enabled',
        function(value, group, controlName) { _this.led5.set(value === 1); });
    engine.makeConnection(this.group, 'beatloop_1_enabled',
        function(value, group, controlName) { _this.led6.set(value === 1); });
    engine.makeConnection(this.group, 'beatloop_2_enabled',
        function(value, group, controlName) { _this.led7.set(value === 1); });
    engine.makeConnection(this.group, 'beatloop_4_enabled',
        function(value, group, controlName) { _this.led8.set(value === 1); });
};

LoopRollPage.prototype.deactivate = function() {
    PageBase.prototype.deactivate.call(this);

    // Release all buttons
    engine.setValue(this.group, 'reverseroll', 0);
    engine.setValue(this.group, 'beatlooproll_0.0625_activate', 0);
    engine.setValue(this.group, 'beatlooproll_0.125_activate', 0);
    engine.setValue(this.group, 'beatlooproll_0.25_activate', 0);
    engine.setValue(this.group, 'beatlooproll_0.5_activate', 0);
    engine.setValue(this.group, 'beatlooproll_1_activate', 0);
    engine.setValue(this.group, 'beatlooproll_2_activate', 0);
    engine.setValue(this.group, 'beatlooproll_4_activate', 0);
};

LoopRollPage.prototype._padBtnMsg = function(padNum, value) {
    if (value === 0x7f) {
        value = 1;
    }
    else {
        value = 0;
    }

    var control;

    switch (padNum) {
    case 1: engine.setValue(this.group, 'reverseroll', value); return;
    case 2: control = 'beatlooproll_0.0625_activate';  break;
    case 3: control = 'beatlooproll_0.125_activate';  break;
    case 4: control = 'beatlooproll_0.25_activate';  break;
    case 5: control = 'beatlooproll_0.5_activate';  break;
    case 6: control = 'beatlooproll_1_activate';  break;
    case 7: control = 'beatlooproll_2_activate';  break;
    case 8: control = 'beatlooproll_4_activate';  break;
    default: throw new Error("unexpected case in switch");
    }

    engine.setValue(this.group, control, value);
};

/**
 * Sampler
 */
const SamplerPage = function(channel, group, globalState) {
    PageBase.call(this, channel, group, globalState);
};

SamplerPage.prototype = Object.create(PageBase.prototype);

SamplerPage.prototype.init = function() {
    PageBase.prototype.init.call(this);
};

SamplerPage.prototype.deactivate = function() {
    PageBase.prototype.deactivate.call(this);

    // release all buttons
    engine.setValue('[Sampler1]', "stop", 1);
    engine.setValue('[Sampler1]', "stop", 0);
    engine.setValue('[Sampler2]', "stop", 1);
    engine.setValue('[Sampler2]', "stop", 0);
    engine.setValue('[Sampler3]', "stop", 1);
    engine.setValue('[Sampler3]', "stop", 0);
    engine.setValue('[Sampler4]', "stop", 1);
    engine.setValue('[Sampler4]', "stop", 0);
    engine.setValue('[Sampler5]', "stop", 1);
    engine.setValue('[Sampler5]', "stop", 0);
    engine.setValue('[Sampler6]', "stop", 1);
    engine.setValue('[Sampler6]', "stop", 0);
    engine.setValue('[Sampler7]', "stop", 1);
    engine.setValue('[Sampler7]', "stop", 0);
    engine.setValue('[Sampler8]', "stop", 1);
    engine.setValue('[Sampler8]', "stop", 0);
};

SamplerPage.prototype._padBtnMsg = function(padNum, value) {
    if (value === 0x7f) {
        value = 1;
    }
    else {
        value = 0;
    }

    var control;

    switch (padNum) {
    case 1: control = '[Sampler1]'; break;
    case 2: control = '[Sampler2]'; break;
    case 3: control = '[Sampler3]'; break;
    case 4: control = '[Sampler4]'; break;
    case 5: control = '[Sampler5]'; break;
    case 6: control = '[Sampler6]'; break;
    case 7: control = '[Sampler7]'; break;
    case 8: control = '[Sampler8]'; break;
    default: throw new Error("unexpected case in switch");
    }

    if (value) {
        engine.setValue(control, "cue_gotoandplay", 1);
        engine.setValue(control, "cue_gotoandplay", 0);
    } else {
        // Stop on release?
        engine.setValue(control, "stop", 1);
        engine.setValue(control, "stop", 0);
    }
};

/**
 * All one all one all one.
 *
 * What exactly do I want on the combo page?
 */
const ComboPage = function(channel, group, globalState) {
    PageBase.call(this, channel, group, globalState);
};

ComboPage.prototype = Object.create(PageBase.prototype);

ComboPage.prototype._padBtnMsg = function(padNum, value) {
};

ComboPage.prototype.init = function() {
    PageBase.prototype.init.call(this);
};


/**
 * Abstraction of one deck 
 *
 * @param globalState references an instance of GlobalState.
 * @param deckNumber is the deck number from 1-4
 */
const Deck = function(globalState, deckNumber) {
    // Base channel for most of what happens on this deck, 
    // zero-indexed (for MIDI). Deck 1 = channel 3 = "0x02"
    var channel = deckNumber + 1;

    /**
     * Reference to global VCI state
     */
    this.globalState = globalState;

    this.deckNumber = deckNumber;
    this.group = "[Channel" + deckNumber + "]";

    // "vinyl" on the deck means "touch to scratch", which is
    // not the same as "external vinyl control"
    this.vinylLed = new LED(channel, 0x06);
    this.miniPadLed1 = new LED(channel, 0x15);
    this.miniPadLed2 = new LED(channel, 0x16);
    this.miniPadLed3 = new LED(channel, 0x17);
    this.miniPadLed4 = new LED(channel, 0x18);

    this.cuePage = new CuePage(channel, this.group, globalState);
    this.loopPage = new LoopPage(channel, this.group, globalState);
    this.loopRollPage = new LoopRollPage(channel, this.group, globalState);
    this.samplerPage = new SamplerPage(channel, this.group, globalState);
    this.comboPage = new ComboPage(channel, this.group, globalState);

    this.currentPage = this.cuePage;
};

Deck.prototype.init = function() {
    this.vinylLed.set(true);
    this.miniPadLed1.set(true);
    this.vinylLed.send();
    this.miniPadLed1.send();
    this.miniPadLed2.send();
    this.miniPadLed3.send();
    this.miniPadLed4.send();

    this.cuePage.init();
    this.loopPage.init();
    this.loopRollPage.init();
    this.comboPage.init();
    this.samplerPage.init();

    this.currentPage = this.cuePage;
    this.currentPage.activate();
    this.currentPage.sendAllLeds();
};

Deck.prototype.handleWheel = function(channel, control, value, status, group) {
    var jogValue = value - 0x40; // Control centers on 0x40 (64)

    if (engine.isScratching(this.deckNumber)) {
        engine.scratchTick(this.deckNumber, jogValue);    // scratch
    }
    else {
        // Correct for high-res wheel (only if not scratching, which is 
        // corrected in the scratchEnble function)

        jogValue = jogValue * JOG_CORRECTION;
        engine.setValue(this.group, 'jog', jogValue);          // Pitch bend (or cue)
    }
};

Deck.prototype.handleWheelTouch = function(channel, control, value, status, group) {
    if (value !== 0) {         // If touched
        var isPlaying = engine.getValue(this.group, "play");

        if (this.vinylLed.isOn    // Scratch if Vinyl is active,
            || (! isPlaying))     // or we're stopped (for cueing)
        {
            engine.scratchEnable(this.deckNumber, 2400 * SCRATCH_TICK_MULTIPLIER, 33.3333, SCRATCH_ALPHA, SCRATCH_BETA, true);
        }
    }
    else {
        engine.scratchDisable(this.deckNumber);
    }
};

Deck.prototype.handleVinyl = function(channel, control, value, status, group) {
    if (value !== 0) {
        this.vinylLed.toggle();
    }
};

/**
 * Handle the C/D deck PFL (serato and probably external-mixer firmware) that 
 * is a switch that ignores midi out.
 */
Deck.prototype.handleCD_PFL = function(channel, control, value, status, group) {

    // If you set the PFL by clicking on the GUI it might get out of sync,
    // but at least this way we'll be in sync if you push the button.

    if (value === 0x7f) { // Is turned on?
        engine.setValue(this.group, 'pfl', 1.0);
    }
    else {
        engine.setValue(this.group, 'pfl', 0.0);
    }
};

/**
 * Load the currently highlighted track into this deck. If shifted, eject.
 */
Deck.prototype.handleLoad = function(channel, control, value, status, group) {
    if (value !== 0x7f) {
        return;
    }

    if (this.globalState.isShifted) {
        engine.setValue(this.group, "eject", 1);
        engine.setValue(this.group, "eject", 0);
    }
    else {
        engine.setValue(this.group, "LoadSelectedTrack", 1);
        engine.setValue(this.group, "LoadSelectedTrack", 0);
    }
};

/**
 * What do we do with the "slip" button? 
 *
 */
Deck.prototype.handleSlipBtn = function(channel, control, value, status, group) {

    // At the moment, "slip" isn't "smart slip" and I don't feel like 
    // implementing that in the controller interface <: so let's make
    // it a "slip until you let go of the button" setup
    //
    // holding shift will lock slip mode until you press and release again.

    if (value === 0x00) {
        // on release, stop slipping, unless "shift" is held
        if (! this.globalState.isShifted) {
            engine.setValue(this.group, "slip_enabled", 0);
        }
    }
    else if (value === 0x7f) {
        // On button down, toggle if shifted, or enable if not
        //
        // However, if it's locked on from a previous shift operation,
        // we want slip to turn off on _press_. Therefore, always
        // toggle state on button-down.
        script.toggleControl(this.group, "slip_enabled");
    }
};

Deck.prototype.handlePageBtn = function(channel, control, value, status, group) {
    if (value !== 0x7f) {
        return;
    }

    this.miniPadLed1.set(false);
    this.miniPadLed2.set(false);
    this.miniPadLed3.set(false);
    this.miniPadLed4.set(false);

    // For simplicity, deactivate all pages every time
    this.cuePage.deactivate();
    this.loopPage.deactivate();
    this.loopRollPage.deactivate();
    this.samplerPage.deactivate();
    this.comboPage.deactivate();

    switch (control) {
    case 0x15: this.currentPage = this.cuePage;      this.miniPadLed1.set(true); break;
    case 0x16: this.currentPage = this.loopPage;     this.miniPadLed2.set(true); break;
    case 0x17: this.currentPage = this.loopRollPage; this.miniPadLed3.set(true); break;
    case 0x18: this.currentPage = this.samplerPage;  this.miniPadLed4.set(true); break;
    default:
        throw new Error("Unexpected control for page button");
    }

    // case 0x17: this.currentPage = this.comboPage;    this.miniPadLed3.set(true); break;

    this.currentPage.activate();
    this.currentPage.sendAllLeds();
};

/**
 * Handle the left encoder, encoder1
 */
Deck.prototype.handleEncoder1 = function(channel, control, value, status, group) {
    // According to an entirely unscientific facebook VCI400 group poll, 
    // and also matching my personal taste, encoders are Pitch and Beatjump.

    value = value << 25 >> 25; // extend to 32-bit
    
    if (this.globalState.isShifted) {   // If shifted, do quarter-tone adjustment
        // THe normal pitch value is 1.0-per-half step so...
       
        value = value / 2;
    }

    var pitch = engine.getValue(this.group, 'pitch_adjust');
    pitch += value;
    engine.setValue(this.group, 'pitch_adjust', pitch);
};

/**
 * Handle the right encoder
 */
Deck.prototype.handleEncoder2 = function(channel, control, value, status, group) {
    // Value is 7-bit 2's complement. Yes, really.
    // Sign-extend it to be 32-bit (java native). This is actually portable
    // and good practice, as far as I can tell. Hah!
    
    value = value << 25 >> 25; // extend to 32-bit 

    if (this.globalState.isShifted) {
        // If a loop is active, adjust the loop size! Otherwise adjust
        // the beatjump size. 
        var isLooping = engine.getValue(this.group, 'loop_enabled');
        if (isLooping) {
            // This will do nothing for a loop that is not quantized
            // we have no way to know whether it is, so, y'know, *shrug*?
            // I mean, we could retrieve the loop size and see if it changed
            // to reflect the loop_halve but come on no we're not doing that.
            
            while (value < 0) {             // Grow
                ++ value;
                engine.setValue(this.group, 'loop_halve', 1);
                engine.setValue(this.group, 'loop_halve', 0);
            }

            while (value > 0) {             // Shrink
                -- value;
                engine.setValue(this.group, 'loop_double', 1);
                engine.setValue(this.group, 'loop_double', 0);
            }
            
        }
        else {
            // Adjust the beatjump size in powers of two
            var bjSize = engine.getValue(this.group, 'beatjump_size');

            while (value < 0) {
                ++ value;
                if (bjSize > 0.125) {       // Seriously you're going to beatjump by 1/8?
                    bjSize = bjSize / 2;
                }
            }

            while (value > 0) {             // Shrink
                -- value;
                if (bjSize < 32) {          // >32 seems unnecessary with a spinny button
                    bjSize = bjSize * 2;
                }
            }
            engine.setValue(this.group, 'beatjump_size', bjSize);
        }
    }
    else {
        while (value < 0) {
            ++ value;
            engine.setValue(this.group, 'beatjump_backward', 1);
            engine.setValue(this.group, 'beatjump_backward', 0);
        }

        while (value > 0) {             // Shrink
            -- value;
            engine.setValue(this.group, 'beatjump_forward', 1);
            engine.setValue(this.group, 'beatjump_forward', 0);
        }
    }
};

Deck.prototype.handleEncoderPush1 = function(channel, control, value, status, group) {
    if (value !== 0x7f) {
        return;
    }

    if (this.globalState.isShifted) {
        engine.setValue(this.group, 'reset_key', 1);
        engine.setValue(this.group, 'reset_key', 0);
    }
    else {
        engine.setValue(this.group, 'sync_key', 1);
        engine.setValue(this.group, 'sync_key', 0);
    }
};

Deck.prototype.handleEncoderPush2 = function(channel, control, value, status, group) {
    if (value !== 0x7f) {
        return;
    }

    // If a loop is enabled, turn it off.
    // If it is *not* enabled, create a beatloop now, at the current location.
    var isEnabled = engine.getValue(this.group, 'loop_enabled');
    if (isEnabled) {
        engine.setValue(this.group, 'reloop_toggle', 1); // Turn it off
        engine.setValue(this.group, 'reloop_toggle', 0);
    }
    else {
        engine.setValue(this.group, 'beatloop_activate', 1);
        engine.setValue(this.group, 'beatloop_activate', 0); // "Release" the button :P
    }
};

/**
 * Handle sync. 
 *
 * A press matches the BMP of the other/master track. If this channel is sync_master,
 * downgrades it to sync_enabled. If sync_enabled, disables it.
 * 
 * Shifted, sets sync_enabled.
 * If sync_enabled is already set, makes this channel sync master.
 */

// For now disable: mixxx is headed towards a traktor-like master sync
// paradigm which is nice, but sometimes the "beatsync_tempo" style button 
// is useful, and I can't figure how to support sync_enabled long-hold 
// programmatically.
/*
Deck.prototype.handleSyncBtn = function(channel, control, value, status, group) {
    // Maybe later I'll give a crap about the push-and-hold-for-sync-master stuff,
    // but TBH all that magic sync stuff is for when you're drunk.
    
    if (value !== 0x7f) {
        return;
    }

    var mode = engine.getValue(this.group, "sync_mode");

    if (this.globalState.isShifted) {
        engine.setValue(this.group, "sync_enabled", 1);
        if (mode === 0) {      // Not enabled?
            engine.setValue(this.group, "sync_mode", 1);
        }
        else { // sync already enabled, make us master
            engine.setValue(this.group, "sync_mode", 2);
        }
    }
    else {  // normal button press
        if (mode >= 2) {      // we are master?
            engine.setValue(this.group, "sync_mode", 1); // unmaster
        }
        else if (mode === 1) { // we are slave?
            engine.setValue(this.group, "sync_enabled", 0); // unsync
        }
        else {                 // we are nothing, match beat the cool way
            engine.setValue(this.group, "sync_enabled", 0);
            engine.setValue(this.group, "beatsync_tempo", 1);
        }
    }
};
*/

const VCI400 = function() {
    this.globalState = new GlobalState();
    this.deck1 = new Deck(this.globalState, 1);
    this.deck2 = new Deck(this.globalState, 2);
    this.deck3 = new Deck(this.globalState, 3);
    this.deck4 = new Deck(this.globalState, 4);

    // Blinkies
    this.sync1 = new TempoLED(0x02, 0x01, '[Channel1]');
    this.sync2 = new TempoLED(0x03, 0x01, '[Channel2]');
    this.sync3 = new TempoLED(0x04, 0x01, '[Channel3]');
    this.sync4 = new TempoLED(0x05, 0x01, '[Channel4]');

    // Master VU meter works OK with the Serato firmware. If you have the "standalone mixer"
    // firmware you will want to disable this.
    this.leftVuController = new VuController(0xBE, 0x2B);
    this.rightVuController = new VuController(0xBE, 0x2C);

    this.deck1Vu = new VuController(0xB2, 0x11);
    this.deck2Vu = new VuController(0xB3, 0x11);
    this.deck3Vu = new VuController(0xB4, 0x11);
    this.deck4Vu = new VuController(0xB5, 0x11);
}

VCI400.prototype.init = function(id) {
    print("VCI400 init...");

    engine.setValue("[App]", "num_decks", 4);

    this.deck1.init();
    this.deck2.init();
    this.deck3.init();
    this.deck4.init();

    this.sync1.init();
    this.sync2.init();
    this.sync3.init();
    this.sync4.init();

    this.leftVuController.init('[Main]', 'vu_meter_left');
    this.rightVuController.init('[Main]', 'vu_meter_right');
    this.deck1Vu.init('[Channel1]', 'vu_meter');
    this.deck2Vu.init('[Channel2]', 'vu_meter');
    this.deck3Vu.init('[Channel3]', 'vu_meter');
    this.deck4Vu.init('[Channel4]', 'vu_meter');

    var _this = this;

    // Connect the sync leds
    engine.makeConnection('[Channel1]', 'sync_mode',
        function(value, group, controlName) { _this.sync1.set(value > 0.5 && value < 2.5); });
    engine.makeConnection('[Channel2]', 'sync_mode',
        function(value, group, controlName) { _this.sync2.set(value > 0.5 && value < 2.5); });
    engine.makeConnection('[Channel3]', 'sync_mode',
        function(value, group, controlName) { _this.sync3.set(value > 0.5 && value < 2.5); });
    engine.makeConnection('[Channel4]', 'sync_mode',
        function(value, group, controlName) { _this.sync4.set(value > 0.5 && value < 2.5); });
    
    // Initialize all lights here?

    // Request current position of all sliders using a magic
    // sysex  F07E7F0601F7 ???
    midi.sendSysexMsg([
        0xF0,
        0x7E,
        0x7F,
        0x06,
        0x01,
        0xF7
    ]);

    print("VCI400 init done");
};

VCI400.prototype.shutdown = function(id) {
    // Lights out

    print("VCI400 shut down complete");
};

VCI400.prototype.handleShift = function(channel, control, value, status, group) {
    if (value === 0x7f) {
        this.globalState.isShifted = true;
    }
    else {
        this.globalState.isShifted = false;
    }
};

/**
 * Connect the FX encoder to "Super1" of the effects rack
 * ... or whatever "group" it is connected to.
 */
VCI400.prototype.handleFxEncoder = function(channel, control, value, status, group) {
    value = value << 25 >> 25; // extend 7-bit 2's to 32-bit 

    var super1 = engine.getValue(group, "super1");
    super1 += 0.04 * value;

    if (super1 < 0) {
        super1 = 0;
    }
    if (super1 > 1) {
        super1 = 1;
    }

    engine.setValue(group, "super1", super1);
};

/**
 * Enable the Browse knob as a navigation control. When shifted, do page-up, -down
 */
VCI400.prototype.handleBrowseTwist = function(channel, control, value, status, group) {
    value = value << 25 >> 25; // extend 7-bit 2's to 32-bit 

    if (this.globalState.isShifted) {
        while (value < 0) {
            ++ value;
            engine.setValue(group, "ScrollUp", 1); // aka "PageUp"
            engine.setValue(group, "ScrollUp", 0); // aka "PageUp"
        }
        while (value > 0) {
            -- value;
            engine.setValue(group, "ScrollDown", 1); // "PageDown"
            engine.setValue(group, "ScrollDown", 0); // "PageDown"
        }
    }
    else {
        while (value < 0) {
            ++ value;
            engine.setValue(group, "MoveUp", 1);
            engine.setValue(group, "MoveUp", 0);
        }
        while (value > 0) {
            -- value;
            engine.setValue(group, "MoveDown", 1);
            engine.setValue(group, "MoveDown", 0);
        }
    }

};

/**
 * Move focus forward, or back if shift is pressed (tab/shift-tab)
 */
VCI400.prototype.handleMoveFocusBtn = function(channel, control, value, status, group) {
    if (value === 0x7f) {
        if (this.globalState.isShifted) {
            engine.setValue(group, "MoveFocusBackward", 1);
            engine.setValue(group, "MoveFocusBackward", 0);
        }
        else {
            engine.setValue(group, "MoveFocusForward", 1);
            engine.setValue(group, "MoveFocusForward", 0);
        }
    }
};

/**
 * Add current selection to autodj bottom, or top if shifted
 */
VCI400.prototype.handleAutoDjAddBtn = function(channel, control, value, status, group) {
    if (value === 0x7f) {
        if (this.globalState.isShifted) {
            engine.setValue(group, "AutoDjAddTop", 1);
            engine.setValue(group, "AutoDjAddTop", 0);
        }
        else {
            engine.setValue(group, "AutoDjAddBottom", 1);
            engine.setValue(group, "AutoDjAddBottom", 0);
        }
    }
};

/**
 * Start/stop AutoDJ, or if shifted trigger Fade Now
 */
VCI400.prototype.handleAutoDjBtn = function(channel, control, value, status, group) {
    if (value === 0x7f) {
        if (this.globalState.isShifted) {
            engine.setValue("[AutoDJ]", "fade_now", 1);
            engine.setValue("[AutoDJ]", "fade_now", 0);
        }
        else {
            script.toggleControl('[AutoDJ]', 'enabled');
        }
    }
};

/**
 * Main VCI400 mapping entry point.
 *
 * Mixxx 2.4.1 can't find this if it is "const". Probably due to the interpreter implementation?
 */
var BHVNVestaxVCI400 = new VCI400();

